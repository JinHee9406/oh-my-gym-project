<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <style>
    
	
    	
        img.ohfooter_logo{
                margin-left: -50%; 
                margin-bottom: 50%; 
                margin-top: 50%; 
            }
            
        #ohfooter{
                background: rgb(245, 245, 245) ; 
                font-family: Noto Sans SC !important;
                font-size: 11px !important; 
                font-weight: 400 !important; 
            }
        p{
                color:rgb(116, 116, 116);
            }

        img.ohfooter-icon{
            margin-right: 0px !important; 
            position : relative !important;
        }
        
  
    </style>
</head>
<body>
    <footer id="ohfooter">
        <hr style="border: solid 1px rgb(218, 218, 218);"><br><br><br>
        <table id="footer_bottom_table">
            <tbody>
                <tr><td class="footer_bottom_td" style="width: 17%; text-align: left; border-left-width: 50px;padding-left: 100px;">
                    <img class="ohfooter_logo" src="/omg/resources/img/OhmyGym_Foot.png"></td>
                    <td style="width: 70%; text-align: left;">
                        <a class="PhoneNum" style="font-weight: bold; font-size: 20px; color: orangered;">010-9974-8184</a><br>
                        <p class="workTime">평일 <b>09:00 ~ 17:00</b><br>
                        (점심시간 <b>13:00 ~ 15:00</b> 제외 · 주말/공휴일 제외)</p><br>
                        <a><img src="/omg/resources/img/footerKh.png" style="width: 100px; height: 30px;"></a><br><br>
                        <a href="" target="_blank" rel="noopener noreferrer" style="text-decoration: underline;">이용약관</a> | 
                        <a href="" target="_blank" rel="noopener noreferrer" style="text-decoration: underline;">개인정보 처리방침</a>
                        <p class="footer_text"><b>오마이짐</b> 은 프리랜서 트레이너와 일반 사용자들을 매칭시켜주는 플랫폼이며 트레이너와 일반 사용자 사이에 대한<br>
                            계약, 또는 트러블 발생시에 대한 책임은 <b>오마이짐</b>이 지지 않습니다.</p>
                        <p class="copyright">상호명: 오마이짐(<b>OH! MY GYM</b>) · 대표이사: 장소이  · 개인정보책임관리자: 장소이  · 주소: 서울특별시 강남구 테헤란로14층 6 남도빌딩 2층 C강의장<br>
                            사업자등록번호: <b>120-88-22326</b> ·  통신판매업신고증: 제 2015-서울강남-0056호 
                            고객센터: 010-9974-8184 · 이메일:soiy0205@gmail.com 

                        <br><b>Copyright © 2020.SoIyJang all rights reserved.</b></p></td>
                        <td style="width: 13%; text-align: right; padding-top: 200px; margin-left: 50px; padding-right: 50px;"><a href="" target="_blank" rel="noopener noreferrer">
                            <img class="ohfooter-icon" src="/omg/resources/img_icon/fakebook_con.png"></a>
                            <a href="https://go.onelink.me/app/f689a567" target="_blank" rel="noopener noreferrer"></a>
                            <img class="ohfooter-icon" src="/omg/resources/img_icon/notub_con.png" style="margin-left: 15px;"></a>
                        </td>
                    </tr>
                </tbody>
            </table>
    </footer>
</body>
</html>